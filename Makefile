.PHONY: build test

build:
	mkdir -p build
	cp main.py build/main.py

test:
	python build/main.py
